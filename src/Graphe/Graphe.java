package Graphe;

import java.util.ArrayList;
import java.util.Collections;

import Jeu.Couleur;
import Jeu.Echiquier;
import Jeu.Piece;
import processing.core.PVector;

public class Graphe {
	
	private Noeud racine;
	
	
	public Graphe() {
		racine =  null;
	}
	public Graphe(Noeud racine_) {
		racine = racine_.clone();
	}
	
	public void setRacine(Noeud racine_) {
		racine = racine_;
	}
	
	public Noeud minMax(Noeud n, int profondeur , boolean maximize) {
		
		if(profondeur == 0) {
			return n;
		}
		
		Noeud choice = null;
		ArrayList<Noeud> successeur = n.getSuccesseur();
		
		Collections.shuffle(successeur);
		
		
		
		if(successeur.isEmpty()) {
			return n;
		}
		
		if(maximize) {
			float max = Integer.MIN_VALUE;
			for (Noeud fils : successeur) {
				
				if(fils.getScore() < max )
					continue;
				
				
				Noeud nextNode = minMax(fils, profondeur-1,!maximize);
				
				
				if(nextNode.getScore() > max || nextNode.getScore() == max && Math.random() < 0.5) {
					max = nextNode.getScore();
					choice = nextNode;
				}
					
			}
			
			
		}else {
			float min = Integer.MAX_VALUE;
			for (Noeud fils : successeur) {
				if(fils.getScore() > min)
					continue;
				
				Noeud nextNode = minMax(fils, profondeur-1, !maximize);
				
				
				if(nextNode.getScore() < min || nextNode.getScore() == min && Math.random() < 0.5) {
					min = nextNode.getScore();
					choice = nextNode;
				}
			}
			
		}
		
		
		return choice;

	}
	
	
	public void constructionGraphe(Echiquier e,Noeud parent,int profondeur,Couleur couleur) {
		
		
		if(profondeur == 0)
			return;
		
		
		// on r�cup�re toute les position des piece de m�me couleur que celui qui joue
		ArrayList<PVector> positionPiece = new ArrayList<PVector>();
		for (int i = 0; i < e.getDimension(); i++) {
			for (int j = 0; j < e.getDimension(); j++) {
				if(e.getColorPiece(i, j) == couleur)
					positionPiece.add(new PVector(i,j));
				
			}
			
		}
		
		
	
		for(PVector pst : positionPiece) {
			int i = (int)pst.x;
			int j = (int) pst.y;
			Piece p = e.getPieceAt(i, j);
			
			ArrayList<PVector> deplacementPiece = e.realCoordForDeplacement(i, j,p.getDeplacementAutoris�());
			
			for(PVector depl : deplacementPiece) {
				
				if(e.deplacementAutoris�(i,j,(int)depl.x ,(int)depl.y)) {
					Echiquier ePrime = new Echiquier(e);
					PVector origine = pst.copy();
					PVector destination = depl.copy();
					
					
					ePrime.deplacer((int)origine.x,(int)origine.y,(int)destination.x,(int)destination.y);
					
					
					float score  = ePrime.getFullScore();
					int vWin = (couleur == Couleur.BLANC) ? -1 : 1;
					boolean checkMate = ePrime.isCheckMate();
					Couleur check = ePrime.isCheck(couleur);
					Piece pSelect = ePrime.modeSelectionPion();
					
					if(check == couleur) {
						score+=2*vWin;
					}else if(check != couleur) {
						score-=2*vWin;
					}
					
					if(checkMate && check == couleur) {
						score += 10*vWin;
					}else if(checkMate && check != couleur) {
						score-= 10*vWin;
					}
					if(pSelect != null) {
						if(pSelect.getColor() == couleur) {
							score-= 8*vWin;
						}else if(pSelect.getColor() != couleur) {
							score+= 8*vWin;
						}
							
					}
					
					
					Noeud fils =  new Noeud(origine,destination,score,parent);
					parent.addSuccesseur(fils);
					
					Couleur nextCouleur = (couleur == Couleur.BLANC) ? Couleur.NOIR : Couleur.BLANC;
					
					constructionGraphe(ePrime,fils, profondeur-1, nextCouleur);
				}
				
			}

		}
		

		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}
	
	public Noeud getRacine() {
		return racine;
	}
	
	
	

}
