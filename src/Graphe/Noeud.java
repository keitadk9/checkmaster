package Graphe;

import java.util.ArrayList;

import processing.core.PVector;

public class Noeud  {
	
	// deplacement � faire pour arriver � la configuration consid�r�
	private PVector start , end;
	private float score;
	private Noeud parent;
	private ArrayList<Noeud> successeur;
	private boolean racine;
	
	public Noeud(PVector start_,PVector end_ ,float score_,Noeud p) {
		successeur = new ArrayList<Noeud>();
		start = start_.copy();
		end = end_.copy();
		score = score_;
		racine = false;
		parent = p;
	}
	
	public Noeud(float scoreI) {
		this(new PVector(),new PVector(),scoreI,null);
		racine = true;
	}
	
	public Noeud(Noeud n) {
		this(n.getStart(),n.getEnd(),n.getScore(),n.parent);
		
		for (Noeud noeud : n.successeur) {
			this.successeur.add(noeud);
		}
	}
	
	
	public PVector getStart() {
		return start.copy();
	}
	public PVector getEnd() {
		return end.copy();
	}
	
	public float getScore() {
		return score;
	}
	
	public void addSuccesseur(Noeud n ) {
		successeur.add(n);
	}
	public Noeud getParent() {
		return parent;
	}
	public Noeud clone() {
		return new Noeud(this);
	}
	
	public ArrayList<Noeud> getSuccesseur(){
		return successeur;
	}
	public boolean isRacine() {
		return racine;
	}
	
	
	
	
}
