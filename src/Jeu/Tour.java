package Jeu;

import processing.core.PVector;

public class Tour extends Piece{
	
	public Tour(Couleur cl,float x_,float y_) {
		label = "Tour";
		valeur = 5.1f;
		image = null;
		c = cl;
		port� = true;
		x = x_;
		y = y_;
	}

	@Override
	public PVector[] getDeplacementAutoris�() {
		
		return new PVector[] {new PVector(1,0),new PVector(-1,0),new PVector(0,1),new PVector(0,-1)};
	}
	

	@Override
	public Piece clone() {
		 return new Tour(c, x, y);
	}
	
	
}
