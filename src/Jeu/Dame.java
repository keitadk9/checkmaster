package Jeu;

import processing.core.PVector;

public class Dame extends Piece{

	
	public Dame(Couleur cl,float x_,float y_) {
		label = "Dame";
		valeur = 8.8f;
		image = null;
		c = cl;
		port� = true;
		x = x_;
		y = y_;
	}

	@Override
	public PVector[] getDeplacementAutoris�() {
		PVector[] deplacement = new PVector[8];
		int indice = 0;
		for(int i = -1 ; i <= 1 ;++i) {
			for(int j = -1 ; j <= 1 ;++j) {
				if(j == 0 && i == 0)
					continue;
				deplacement[indice++] = new PVector(i,j); 
			}
		}
		return deplacement;
	}
	
	@Override
	public Piece clone() {
		 return new Dame(c, x, y);
	}
	

	
	
}
