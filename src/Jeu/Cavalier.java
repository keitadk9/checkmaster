package Jeu;

import processing.core.PVector;

public class Cavalier extends Piece{

	public Cavalier(Couleur cl,float x_,float y_) {
		label = "Cavalier";
		valeur = 3.2f;
		image = null;
		c = cl;
		port� = false;
		x = x_;
		y = y_;
	}

	@Override
	public PVector[] getDeplacementAutoris�() {
		return new PVector[] {
				new PVector(2,1),new PVector(2,-1),
				new PVector(1,2),new PVector(-1,2),
				new PVector(-2,1),new PVector(-2,-1),
				new PVector(1,-2),new PVector(-1,-2)
		};
		
	}

	@Override
	public Piece clone() {
		 return new Cavalier(c, x, y);
	}
	

	
	
}
