package Jeu;
import IA.CheckMaster;
import Joueurs.*;
import processing.core.PApplet;
import processing.core.PImage;
public class Jeu {
	
	private Echiquier plateau;
	private Player joueurB,joueurN;
	int tour;
	private boolean vsIA;
	private Piece pionSelection;
	
	
	
	public Jeu(Echiquier e,String nomB,String nomN){
		joueurB = new Player(nomB,Couleur.BLANC);
		joueurN = new Player(nomN,Couleur.NOIR);
		plateau = e;
		vsIA = false;
		tour = 1;
		pionSelection = null;
	}
	public Jeu(Echiquier e,String nom,Couleur c) {
		
		if(c == Couleur.BLANC) {
			joueurB = new Player(nom, c);
			joueurN = new CheckMaster(Couleur.NOIR,e);
		}else {
			joueurN = new Player(nom, c);
			joueurB = new CheckMaster(Couleur.BLANC,e);
		}
		vsIA = true;
		plateau = e;
		tour = 1;
		
		
	}
	
public Jeu(Echiquier e) {
		
	
		joueurB = new CheckMaster(Couleur.BLANC,e);
		joueurN = new CheckMaster(Couleur.NOIR,e);
		
		vsIA = true;
		plateau = e;
		tour = 1;
		
		
	}
	
	public void update() {
		iAchoice();
	}
	
	
	
	
	
	public void iAchoice() {	
		if(joueurB.getClass().equals(CheckMaster.class) && getCouleurTour() == joueurB.getCouleur()) {
			//System.out.println("Joueur B");
			((CheckMaster) joueurB).play(plateau);
			pionSelection = plateau.modeSelectionPion();
			if(pionSelection == null) 
				tour++;
			
		}
		
		else if(joueurN.getClass().equals(CheckMaster.class) && getCouleurTour() == joueurN.getCouleur()) {
			//System.out.println("Joueur N");
			((CheckMaster) joueurN).play(plateau);
			pionSelection = plateau.modeSelectionPion();
			if(pionSelection == null) 
				tour++;
			
		}
		
		
	}
	
	public void iASelect(PImage[] imagesPiece) {
		if(joueurB instanceof CheckMaster && getCouleurTour() == joueurB.getCouleur()) {
			((CheckMaster) joueurB).selectPionTransformation(plateau,imagesPiece);
			
			
		}
		
		else if(joueurN instanceof CheckMaster && getCouleurTour() == joueurN.getCouleur()) {
			((CheckMaster) joueurN).selectPionTransformation(plateau,imagesPiece);
			
		}
	}
	
	
	public void display(PApplet p) {
		plateau.display(p);
	}
	
	public void deplacer(int i,int j , int iF,int jF) {
		
		if(plateau.getColorPiece(iF, jF) != plateau.getColorPiece(i, j)) {
			if(plateau.deplacer(i, j, iF, jF)) {
				pionSelection = plateau.modeSelectionPion();
				if(pionSelection == null) 
					tour++;
				
					
			}
			
		}
	}
	public void transformationPiece(int i,int j,Piece nPiece) {
		plateau.transformationPiece(i,j,nPiece);
	}
	public boolean IsIaTurn() {
		if(joueurB instanceof CheckMaster && getCouleurTour() == joueurB.getCouleur()) {
			return true;
		}
		
		else if(joueurN instanceof CheckMaster && getCouleurTour() == joueurN.getCouleur()) {
			return true;
		}
		return false;
	}
	
	public boolean hasPiece(int i ,int j) {
		return plateau.hasPiece(i,j);
	}
	
	public void setCaseTraversé(int i,int j,boolean b) {
		plateau.setCaseTraversé(i, j, b);
	}
	public boolean indexValide(int i ,int j) {
		return plateau.indexValide(i, j);
	}
	
	public boolean isCheck() {
		return plateau.isCheck(null) != null;
	}
	public int getTour() {
		return tour;
	}
	
	public Couleur getCouleurTour() {
		return (tour % 2 == 0 ) ? Couleur.NOIR : Couleur.BLANC;
	}
	
	public Couleur getCouleurPiece(int i, int j) {
		return plateau.getColorPiece(i, j);
	}
	
	public float getScore() {
		return plateau.getFullScore();
	}
	public boolean IsVsIA() {
		return vsIA;
	}
	
	public Echiquier getEchiquier() {
		return plateau.clone();
	}
	
	public void disabledmodeSelectionPion() {
			pionSelection = null;
			tour++;
	}
	public boolean isModeSelectionPion() {
		return pionSelection != null;
	}
	
	public Couleur getCouleurPionSelection() {
		return pionSelection.getColor();
	}
	
	public Piece getPieceSelection() {
		return pionSelection.clone();
	}
	
	public Piece getPiece(int i,int j) {
		if(plateau.getPieceAt(i, j) != null)
			return plateau.getPieceAt(i, j).clone();
		
		return null;
	}
	
	public boolean ischeckMate() {
		return plateau.isCheckMate();
	}
	
	
}
