package Jeu;

import processing.core.PVector;

public class Pion extends Piece{
	
	public Pion(Couleur cl,float x_ , float y_) {
		label = "Pion";
		valeur = 1;
		image = null;
		c = cl;
		port� = false;
		start = false;
		x = x_;
		y = y_;
	}
	
	public Pion(Pion p) {
		this(p.c,p.x,p.y);
		image = p.image;
		start = p.start;
		
	}
	
	public PVector[] getDeplacementAutoris�() {
		//x = i , y = j 
		if(c == Couleur.BLANC && !start) {
			return new PVector[] {new PVector(-1,-1),new PVector(-2,0),new PVector(-1,0),new PVector(-1,1)};
		}else if(c == Couleur.BLANC && start) {
			return new PVector[] {new PVector(-1,-1),new PVector(-1,0),new PVector(-1,1)};
		}else if(c == Couleur.NOIR && !start) {
			return new PVector[] {new PVector(1,-1),new PVector(2,0),new PVector(1,0),new PVector(1,1)};
		}else if(c == Couleur.NOIR && start) {
			return new PVector[] {new PVector(1,-1),new PVector(1,0),new PVector(1,1)};
		}
		
		return null;
		
	}
	@Override
	public Piece clone() {
		 return new Pion(this);
	}
	
	
	
	
}
