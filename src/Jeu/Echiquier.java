package Jeu;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

import processing.core.PApplet;
import processing.core.PImage;
import processing.core.PVector;

public class Echiquier {
	
	public static final int dimension = 8 ;
	private Case[][] echiquier;
	private float taille;
	private float score;//valeur qui va servir ŕ l'IA plus c'est négatif plus c'est lui qui gagne
	private Couleur lastCheck;
	
	
	public Echiquier(float taille,PImage[] imagesPieces) {
		
		echiquier = new Case[dimension][dimension];
		this.taille = taille;
		lastCheck = null;
		score = 0;
		try {
			setConfiguration("Donnees/Configuration/ConfigBase.txt",imagesPieces);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public Echiquier(Echiquier e) {
		echiquier = new Case[dimension][dimension];
		
		for (int i = 0; i < e.echiquier.length; i++) {
			for (int j = 0; j < e.echiquier.length; j++) {
				echiquier[i][j] = e.echiquier[i][j].clone();
			}
		}
		this.taille = e.taille;
		score = e.score;
	}
	
	
	public void display(PApplet p) {
		for (int i = 0; i < echiquier.length; i++) {
			for (int j = 0; j < echiquier[0].length; j++) {
				echiquier[i][j].displayCase(p);
			}
		}
		
		for (int i = 0; i < echiquier.length; i++) {
			for (int j = 0; j < echiquier[0].length; j++) {
				echiquier[i][j].displayPiece(p);
			}
		}
		
	}
	private void setConfiguration(String chemin,PImage[] imagesPieces)  throws FileNotFoundException{
		
		try {
			@SuppressWarnings("resource")
			Scanner fichier = new Scanner(new FileInputStream(chemin));
			
			int cpt = 0;
			while(fichier.hasNext()) {
				
				int piece = fichier.nextInt();
				int valeur = piece%6 ;
				int side = piece / 6;
				Couleur c = (side == 0) ? Couleur.NOIR : Couleur.BLANC;
				Piece p;

				int i = cpt / echiquier[0].length;
				int j = cpt % echiquier[0].length;
				
				float x = j*taille;
				float y = i*taille;
				switch(valeur){
					
					case 0:
						p = new Pion(c,x,y);
						break;
					case 1: 
						p = new Cavalier(c,x,y);
						break;
					case 2 :
						p = new Dame(c,x,y);
						break;
					case 3 :
						p = new Fou(c,x,y);
						break;
					case 4 :
						p = new Roi(c,x,y);
						break;
					case 5 :
						p = new Tour(c,x,y);
						break;
					default:
						p = null;	
				}
				
				
				if(p != null)
					p.setImage(imagesPieces[Pieces.values()[valeur].ordinal()+side*6]);
				
				echiquier[i][j] = new Case(p,false,j*taille,i*taille,taille);
				cpt++;
			}
			fichier.close();
			
		
		}catch( FileNotFoundException e) {
			System.err.println("Fichier introuvable");
		}
	}
	
	public void setCaseTraversé(int i,int j,boolean traversé) {
		Piece p = echiquier[i][j].getPiece();

		ArrayList<PVector> deplacement = realCoordForDeplacement(i, j,p.getDeplacementAutorisé());
		
		for (PVector vecteur : deplacement) {
				if(!deplacementAutorisé(i, j,(int)vecteur.x, (int)vecteur.y)) 
					continue;
				
				
				boolean memeCouleur = echiquier[(int)vecteur.x][(int)vecteur.y].getColor() == p.getColor();
				
				if(!memeCouleur)
					echiquier[(int)vecteur.x][(int)vecteur.y].setTraversé(traversé,false);				
		}
		echiquier[i][j].setTraversé(traversé,(traversé) ? true : false);
	}
		
		
	
	public boolean deplacer(int i,int j , int iF,int jF) {
		
		if(deplacementAutorisé(i, j, iF, jF)) {
			//A changer dépend de la couleur choisie par le joueur 
			float point = echiquier[iF][jF].getPoint() * ((echiquier[iF][jF].getColor() == Couleur.BLANC) ? -1 : 1);
			
			echiquier[iF][jF].setPiece(echiquier[i][j].remove());
			
			if(isCheck(echiquier[iF][jF].getColor()) == echiquier[iF][jF].getColor() ) {
				echiquier[i][j].setPiece(echiquier[iF][jF].remove());
				return false;
			}
			if(!echiquier[iF][jF].getPiece().hasStart())
				echiquier[iF][jF].getPiece().start();
			
			score += point;
			return true;
			
		}
		return false;
	}
	
	public boolean deplacementAutorisé(int i ,int j, int iF,int jF) {
		if(deplacementValide(i, j, iF, jF)) {
			if(echiquier[iF][jF].hasPiece() && echiquier[iF][jF].getColor() == echiquier[i][j].getColor() )
				return false;
			
			
			//Couleur enEchec =  isCheck();
			
			
			Piece p = echiquier[i][j].remove();
			Piece p2 = echiquier[iF][jF].remove();
			
			echiquier[iF][jF].setPiece(p);
			
			
			Couleur isCheck = isCheck(p.getColor());
			if(isCheck == p.getColor() ) {
				echiquier[i][j].setPiece(p);
				echiquier[iF][jF].setPiece(p2);
				return false;
			}
			echiquier[i][j].setPiece(p);
			echiquier[iF][jF].setPiece(p2);
			return true;
		}
		return false;
	}
	public boolean deplacementValide(int i,int j , int iF,int jF) {
		
		
		ArrayList<PVector> coordValide = realCoordForDeplacement(i, j, echiquier[i][j].getPiece().getDeplacementAutorisé());
		
		for (PVector pVector : coordValide) {
			if(pVector.equals(new PVector(iF,jF))) {
				return true;
			}
		}
		return false;
	}
	
	public boolean hasPiece(int i ,int j) {
		return echiquier[i][j].hasPiece();
	}
	
	public Piece getPieceAt(int i ,int j) {
		return echiquier[i][j].getPiece();
	}
	public Couleur getColorPiece(int i ,int j) {
		return echiquier[i][j].getColor();
	}
	
	public boolean indexValide(int i ,int j) {
		return i>=0  && i < echiquier.length && j>=0 && j < echiquier[0].length;
	}
	
	
	
	public Couleur isCheck(Couleur couleur) {
		
		Couleur[] checkCouleur = new Couleur[2];
		int indice = 0;
		
		for (int i = 0; i < echiquier.length && indice < 2; i++) {
			for (int j = 0; j < echiquier[i].length && indice < 2; j++) {
				Case c = echiquier[i][j];
				if(c.hasPiece()) {
					Couleur couleurP = c.getColor();
					ArrayList<PVector> deplacementAutorisé = realCoordForDeplacement(i, j,c.getPiece().getDeplacementAutorisé());
					
					for (PVector pVector : deplacementAutorisé) {
						Case cSearch = echiquier[(int)pVector.x][(int)pVector.y];
						if(cSearch.hasPiece()) {
							if(cSearch.getPiece().getClass().equals(Roi.class) && cSearch.getColor() != couleurP) {
								
								if(indice == 0) {
									checkCouleur[indice++] = cSearch.getColor();
								}
								if(indice == 1 ) {
									if(checkCouleur[0] != cSearch.getColor()){
										checkCouleur[indice++] = cSearch.getColor();
										break;
									}
								}
								
								
							}
						}
							
					}
					
				}
				
			}
		}
		if(indice == 0) {
			lastCheck = null;
			return null;
		}
		else if(indice == 1) {
			lastCheck = checkCouleur[0];
			return checkCouleur[0];
		}
		// si on a un conflit (les joueurs sont en échec) on return la couleur de celui qui était en échec au tour d'avant
		else {
			return couleur ;
			
		}
		
		
		
	}
	public boolean isCheckMate() {
		Couleur couleur = isCheck(null);
		if(couleur == null)
			return false;
		
		
		for (int i = 0; i < echiquier.length; i++) {
			for (int j = 0; j < echiquier[i].length; j++) {
				Case c = echiquier[i][j];
				if(c.hasPiece()) {
					if(c.getColor() == couleur) {
						ArrayList<PVector> deplacementAutorisé = realCoordForDeplacement(i, j,c.getPiece().getDeplacementAutorisé());
						for (PVector pVector : deplacementAutorisé) {
							if(deplacementAutorisé(i, j,(int)pVector.x,(int)pVector.y))
								return false;
						}

					}
			
				}
					
			}
				
		}
		
		return true;
	}
	
	
	
	
	public ArrayList<PVector> realCoordForDeplacement(int i ,int j,PVector[] deplacement) {
		boolean infinity = echiquier[i][j].getPiece().getPorté();
		
		ArrayList<PVector> coord = new ArrayList<PVector>();
		
		for (int x = 0 ; x < deplacement.length ; x++) {

			PVector position = new PVector(i,j);
			position.add(deplacement[x]);
			
			if(infinity) {
				while(indexValide((int)position.x,(int) position.y)) {
					coord.add(position.copy());
					if(echiquier[(int)position.x][(int)position.y].hasPiece()) {
						break;
					}
					
					position.add(deplacement[x]);	
				}
			}
			else {
				if(indexValide((int)position.x,(int)position.y)) {
					
					// Partie horrrible ŕ changer plus tard si pas trop la flemme
					if(echiquier[i][j].getPiece().getClass().equals(Pion.class)) {
						
						if(Math.abs(deplacement[x].x) == 2){
							if( !echiquier[(int)position.x][(int)position.y].hasPiece() && !echiquier[(int) ((int)position.x-(deplacement[x].x/Math.abs(deplacement[x].x)))][(int)position.y].hasPiece()){
								coord.add(PVector.add(new PVector(i,j),deplacement[x]));
							}
							
						}else {
						
							if(deplacement[x].y != 0 && echiquier[(int)position.x][(int)position.y].hasPiece()  || deplacement[x].x != 0 && deplacement[x].y == 0 && !echiquier[(int)position.x][(int)position.y].hasPiece())
								coord.add(PVector.add(new PVector(i,j),deplacement[x]));
						}
					}
					
					else {//code pour le cavalier
						coord.add(PVector.add(new PVector(i,j),deplacement[x]));
					}
				}
				
			}
		}
		return coord;	
	}
	
	public Piece modeSelectionPion() {
		
		for (int i = 0; i < echiquier.length; i++) {
			for (int j = 0; j < echiquier[0].length; j++) {
				Piece p = getPieceAt(i, j);
				if(p == null)
					continue;
				if( p.getClass().equals(Pion.class)) {
					
					if(p.getColor() == Couleur.BLANC && i == 0 || p.getColor() == Couleur.NOIR && i == dimension-1) {
						return p;
					}	
				}
			}
		}
		
		return null;
	}
	
	public void transformationPiece(int i,int j,Piece nPiece) {
		echiquier[i][j].setPiece(nPiece);	
	}
	public boolean isModeSelection() {
		return modeSelectionPion() != null;
	}
	public float getPoint(int i ,int j) {
		return echiquier[i][j].getPoint();
	}
	
	public float getFullScore() {
		return score;
	}
	
	public Echiquier clone() {
		return new Echiquier(this);
	}
	
	public int getDimension() {
		return dimension;
	}
	

	
	
	
}
