package Jeu;

import processing.core.PVector;

public class Fou extends Piece{
		
	public Fou(Couleur cl,float x_,float y_) {
		label = "Fou";
		valeur = 3.33f;
		image = null;
		c = cl;
		port� = true;
		x = x_;
		y = y_;
	}

	@Override
	public PVector[] getDeplacementAutoris�() {
		return new PVector[] {new PVector(1,1),new PVector(1,-1),new PVector(-1,1),new PVector(-1,-1)};
				
		
	}
	@Override
	public Piece clone() {
		 return new Fou(c, x, y);
	}
	
	
	
}
