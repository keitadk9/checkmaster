package Jeu;

import processing.core.PApplet;
import processing.core.PImage;
import processing.core.PVector;

public abstract class Piece {
	
	protected String label;
	protected float valeur;
	protected PImage image;
	protected Couleur c;
	protected boolean port�; // vrai si infiny, faux si limit�
	protected boolean start;
	protected float x, y;
	
	public void display(PApplet p,float x,float y) {
		float distance = Math.abs(PApplet.dist(this.x,this.y,x,y));
		float distanceMax = (float)Math.sqrt(Math.pow(800,2)+Math.pow(800,2));
		if( distance > 2f){
			PVector v = PVector.sub(new PVector(x,y),new PVector(this.x,this.y));
			float vitesse = PApplet.map(distance, 0, distanceMax, 1, 30f);
			v.setMag(vitesse);		
			this.x += v.x;
			this.y+= v.y;
		}
		
		
		p.fill(255);
		p.textSize(20);
		if(image == null)
			p.text(label,this.x,this.x);
		else
			p.image(image,this.x, this.y);
		
	}
	public abstract PVector[] getDeplacementAutoris�();
	
	public void setImage(PImage image) {
		this.image = image;
	}
	public boolean hasImage() {
		return image != null;
	}
	
	public boolean getPort�() {
		return port�;
	}
	
	public Couleur getColor() {
		return c;
	}
	public void start() {
		start = true;
	}
	public boolean hasStart() {
		return start;
	}
	public float getPoint() {
		return valeur;
	}
	public float getX() {
		return x;
	}
	public float getY() {
		return y;
	}
	
	public abstract Piece clone();

}
