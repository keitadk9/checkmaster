package Jeu;

import processing.core.PVector;

public class Roi extends Piece{

	public Roi(Couleur cl,float x_,float y_) {
		label = "Roi";
		valeur = 10;
		image = null;
		c = cl;
		port� = false;
		x = x_;
		y = y_;
	}

	@Override
	public PVector[] getDeplacementAutoris�() {
		PVector[] deplacement = new PVector[8];
		int indice = 0;
		for(int i = -1 ; i <= 1 ;++i) {
			for(int j = -1 ; j <= 1 ;++j) {
				if(j == 0 && i == 0)
					continue;
				deplacement[indice] = new PVector(i,j); 
				indice++;
			}
		}
		return deplacement;
	}
	
	@Override
	public Piece clone() {
		 return new Roi(c, x, y);
	}
}
