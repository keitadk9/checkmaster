package Jeu;

import processing.core.PApplet;

public class Case {
	
	private Piece piece ;
	private boolean traversé;
	private boolean tireur;
	private float x,y,taille;
	
	public Case(float x,float y,float taille) {
		this(null,false,x,y,taille);		
	}
	public Case(Piece p ,boolean t,float x,float y,float taille) {
		this.piece = p ;
		traversé = t ;
		this.x = x;
		this.y = y;
		this.taille = taille;
		tireur = false;
		
	}
	
	public Case(Case c) {
		
		
		this(c.getPiece().clone(),c.tireur,c.x,c.y,c.taille);
		
	}
	
	public void displayCase(PApplet p ) {
		
		int indice = (int) ((y/taille) + (x/taille));
		
		if(indice%2 == 0)
			p.fill(200);
		else
			p.fill(50);
		

		if(traversé && piece == null || tireur)
			p.fill(0,200,0,100);
		else if(traversé && piece != null)
			p.fill(255,0,0,100);
		
		p.rect(x,y, taille,taille);
		
		
		
		
		
		
	}
	public void displayPiece(PApplet p) {
		
		if(piece != null)
			piece.display(p, x, y);
	}
	
	public Piece remove() {
		Piece p = piece;
		tireur = false;
		piece= null;
		return p;
	}
	public Piece getPiece() {
		return piece;
	}
	public void setPiece(Piece p) {
		piece = p; 
	}
	
	public boolean hasPiece() {
		return piece != null;
	}
	
	public void setTraversé(boolean b,boolean t) {
		traversé = b;
		tireur = t;
	}
	public boolean isTraversé() {
		return traversé;
	}
	public Couleur getColor() {
		if(piece!= null) {
			return piece.getColor();
		}
		return null;
	}
	
	public float getPoint() {
		
		return (piece == null) ? 0 : piece.getPoint();
		
	}
	
	
	public Case clone() {
		if(piece != null)
			return new Case(piece.clone(),tireur,x,y,taille);

		return new Case(null,tireur,x,y,taille);
		
	}
	
	
}
