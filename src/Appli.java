import Jeu.Cavalier;
import Jeu.Couleur;
import Jeu.Dame;
import Jeu.Fou;
import Jeu.Tour;
import Jeu.Echiquier;
import Jeu.Jeu;
import Jeu.Piece;
import Jeu.Pieces;
import processing.core.PApplet;
import processing.core.PImage;

public class Appli extends PApplet{
	
	Jeu game;
	public static PImage[] imagesPiece;
	int iSelect,jSelect;
	int iDest,jDest;
	float iaThinkDuration = 75;
	float tailleImage = 120f;
	boolean fin = false;
	
	public static void main(String[] args) {
		PApplet.main("Appli");
	}
	
	public void settings() {
		size(1200,800);
		
	}
	
	public void setup() {
		iSelect = jSelect = iDest = jDest -1;
		
		imagesPiece = new PImage[12];
		for (int i = 0; i < imagesPiece.length/2; i++) {
			
			imagesPiece[i] = loadImage("Donnees/Images/"+Pieces.values()[i]+"N.png");
			imagesPiece[i+6] = loadImage("Donnees/Images/"+Pieces.values()[i]+"B.png");
			
		}
		
		game = new Jeu(new Echiquier(100f,imagesPiece),"Dabi",Couleur.BLANC);
	}
	
	public void draw() {
		background(50);
		if(game.isModeSelectionPion()) {
			Couleur c = game.getCouleurPionSelection();
			int offset = (c == Couleur.BLANC) ? 6 : 0;
			image(imagesPiece[1+offset],width-320,400);
			image(imagesPiece[2+offset],width-200,400);
			image(imagesPiece[3+offset],width-320,500);
			image(imagesPiece[5+offset],width-200,500);
		}
			
		fin = game.ischeckMate();
		
		if(game.IsIaTurn() && !game.isModeSelectionPion() && !fin) {
			iaThinkDuration--;
			if(iaThinkDuration < 0) {
				game.iAchoice();
			iaThinkDuration = 110;
			}
		}else if(game.IsIaTurn() && game.isModeSelectionPion() && !fin) {
			iaThinkDuration--;
			if(iaThinkDuration < 0) {
				game.iASelect(imagesPiece);
				game.disabledmodeSelectionPion();
				
				iaThinkDuration = 110;
			}
			
		}
		
		game.display(this);
		textSize(30);
		fill(255);
		text("Tour: "+game.getTour(),width-240,75);
		text("Tour des "+game.getCouleurTour().toString().toLowerCase()+"s"	,width-300,120);
		text("Score du jeu: "+game.getScore(),width-320,180);
		
		if(game.isCheck()) {
			if (game.ischeckMate()) {
				textSize(20);
				text("Les "+game.getCouleurTour().toString().toLowerCase()+"s sont échec et math",width-350,250);
				
			}else {
				text("Les "+game.getCouleurTour().toString().toLowerCase()+"s sont en échec",width-370,250);
			}
		}
		
	}
	
	public void mousePressed() {
		if(iSelect == -1 && jSelect == -1) {
			
		
			if(game.isModeSelectionPion()) {
				
				Piece ancien = game.getPieceSelection();
				int offset = (ancien.getColor() == Couleur.BLANC) ? 1 : 0;
				if(dist(mouseX,mouseY,width-320+tailleImage/2,400+tailleImage/2) < tailleImage/2) {
					Piece nouveau = new Cavalier(ancien.getColor(),ancien.getX(),ancien.getY());
					
					
					nouveau.setImage(imagesPiece[Pieces.Cavalier.ordinal()+offset*6]);
					game.transformationPiece(round(ancien.getY()/100),round(ancien.getX()/100),nouveau);
					game.disabledmodeSelectionPion();
				}
				
				else if(dist(mouseX,mouseY,width-200+tailleImage/2,400+tailleImage/2) < tailleImage/2) {
					Piece nouveau = new Dame(ancien.getColor(),ancien.getX(),ancien.getY());

					nouveau.setImage(imagesPiece[Pieces.Dame.ordinal()+offset*6]);
					game.transformationPiece(round(ancien.getY()/100),round(ancien.getX()/100),nouveau);
					game.disabledmodeSelectionPion();
				}
				
				else if(dist(mouseX,mouseY,width-320+tailleImage/2,500+tailleImage/2) < tailleImage/2) {
					Piece nouveau = new Fou(ancien.getColor(),ancien.getX(),ancien.getY());

					nouveau.setImage(imagesPiece[Pieces.Fou.ordinal()+offset*6]);
					game.transformationPiece(round(ancien.getY()/100),round(ancien.getX()/100),nouveau);
					game.disabledmodeSelectionPion();
				}
				
				else if(dist(mouseX,mouseY,width-200+tailleImage/2,500+tailleImage/2) < tailleImage/2) {
					Piece nouveau = new Tour(ancien.getColor(),ancien.getX(),ancien.getY());

					nouveau.setImage(imagesPiece[Pieces.Tour.ordinal()+offset*6]);
					
					game.transformationPiece(round(ancien.getY()/100),round(ancien.getX()/100),nouveau);
					game.disabledmodeSelectionPion();
				}
				
				return;
			}
			iSelect = (mouseY/100) ;
			jSelect = (mouseX/100) ;
			if(game.indexValide(iSelect, jSelect)) {
				if(game.hasPiece(iSelect,jSelect)) {
					
					if(game.getCouleurTour() == game.getCouleurPiece(iSelect, jSelect))
							game.setCaseTraversé(iSelect,jSelect, true);
					else {
						iSelect = -1;
						jSelect = -1;
					}
					
				}
					
				else {
					iSelect = -1;
					jSelect = -1;
				
				}
			}else {
				iSelect = -1;
				jSelect = -1;
			
			}
				
		}else {
			iDest = (mouseY/100) ;
			jDest = (mouseX/100) ;
			if(game.indexValide(iDest,jDest)) {
				game.setCaseTraversé(iSelect,jSelect, false);
				if(iDest != iSelect || jSelect != jDest) {
					game.deplacer(iSelect,jSelect,iDest,jDest);
				}
			}
			iSelect = -1;
			jSelect = -1;
			iDest = -1;
			jDest = -1;
		}
	}
	
	public static PImage[] getImagesPion() {
		return imagesPiece;
	}
	
	
	
		
		
		
		
	
}
