package Joueurs;

import Jeu.Couleur;

public class Player {

	protected String nom;
	protected Couleur couleur;
	
	public Player(String nom,Couleur c) {
		this.nom = nom;
		couleur = c;
	}
	
	
	public Couleur getCouleur() {
		return couleur;
	}
	
	
	
	
}
