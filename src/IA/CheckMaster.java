package IA;

import java.applet.Applet;
import java.util.ArrayList;

import Graphe.Graphe;
import Graphe.Noeud;
import Jeu.Case;
import Jeu.Cavalier;
import Jeu.Couleur;
import Jeu.Dame;
import Jeu.Echiquier;
import Jeu.Fou;
import Jeu.Piece;
import Jeu.Pieces;
import Jeu.Tour;
import Joueurs.Player;
import processing.core.PImage;
import processing.core.PVector;

public class CheckMaster extends Player {
	
	private Graphe cerveau;
	
	
	public CheckMaster(Couleur c,Echiquier e) {
		super("Daiku",c);
		cerveau = new Graphe(new Noeud(e.getFullScore()));
	}
	
	public void play(Echiquier e) {
		
		
		Noeud racine = new Noeud(e.getFullScore());
		
		cerveau.setRacine(racine);
		cerveau.constructionGraphe(e,cerveau.getRacine(),3,couleur);
		
		boolean side = (couleur == Couleur.BLANC) ? true : false;
		Noeud choix =cerveau.minMax(cerveau.getRacine(),3,side);
		
		//System.out.println("Valeur remont�: "+choix.getScore());
		while(choix.getParent().getParent() != null ) {
			choix = choix.getParent();
		}
		//System.out.println("Valeur remont�: "+choix.getScore()+" s: "+choix.getStart()+" e: "+choix.getEnd() );
		
		
		
		e.deplacer((int)choix.getStart().x,(int)choix.getStart().y, (int)choix.getEnd().x,(int)choix.getEnd().y);

		
	}
	
	public void playRandom(Echiquier e) {
		
		ArrayList<PVector> positions = new ArrayList<PVector>();
		
		// on r�cup�re la position de toutes les pieces qui appartienne � L'IA
		for (int i = 0; i < e.getDimension(); i++) {
			for (int j = 0; j <  e.getDimension(); j++) {
				if(e.getColorPiece(i, j) == couleur ) {
					positions.add( new PVector(i,j));
				}
			}
		}
		
		
		
		ArrayList<PVector[]> deplacement = new ArrayList<PVector[]>();
		
		for (PVector v : positions) {
			
			ArrayList<PVector> deplacementPossible = e.realCoordForDeplacement((int)v.x,(int)v.y,e.getPieceAt((int)v.x,(int)v.y).getDeplacementAutoris�());
			
			for (PVector pVectors : deplacementPossible) {
				if(e.deplacementAutoris�((int)v.x, (int)v.y,(int)pVectors.x,(int)pVectors.y))
					deplacement.add(new PVector[] {v,pVectors});
			}
		}
		//System.out.println("Nombre de choix de l'IA: "+deplacement.size());
		int indicechoix =(int) ((float)Math.random() * deplacement.size());
		PVector[] choix = deplacement.get(indicechoix);
		
		
		e.deplacer((int)choix[0].x,(int) choix[0].y,(int)choix[1].x,(int)choix[1].y);
		
		
	}
	
	public void selectPionTransformation(Echiquier e,PImage[] imagesPiece) {
		
		Piece ancien = e.modeSelectionPion();
		int offset = (ancien.getColor() == Couleur.BLANC) ? 1 : 0;
		float pourcent = (float) Math.random() *2;
		if(pourcent >=1) {
			Piece nouveau = new Dame(ancien.getColor(),ancien.getX(),ancien.getY());

			nouveau.setImage(imagesPiece[Pieces.Dame.ordinal()+offset*6]);
			e.transformationPiece(Math.round(ancien.getY()/100),Math.round(ancien.getX()/100),nouveau);
		}
		if(pourcent < 0.3) {
			Piece nouveau = new Cavalier(ancien.getColor(),ancien.getX(),ancien.getY());

			nouveau.setImage(imagesPiece[Pieces.Cavalier.ordinal()+offset*6]);
			e.transformationPiece(Math.round(ancien.getY()/100),Math.round(ancien.getX()/100),nouveau);
			
		}else if(pourcent >= 0.3 && pourcent < 0.6) {
			Piece nouveau = new Fou(ancien.getColor(),ancien.getX(),ancien.getY());

			nouveau.setImage(imagesPiece[Pieces.Fou.ordinal()+offset*6]);
			e.transformationPiece(Math.round(ancien.getY()/100),Math.round(ancien.getX()/100),nouveau);
		
		}else if(pourcent >= 0.6 && pourcent < 1) {
			Piece nouveau = new Tour(ancien.getColor(),ancien.getX(),ancien.getY());

			nouveau.setImage(imagesPiece[Pieces.Tour.ordinal()+offset*6]);
			e.transformationPiece(Math.round(ancien.getY()/100),Math.round(ancien.getX()/100),nouveau);
		
		}
		
		
		
		
	}

	
	
	
	
}
